﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using lab8.Models;

namespace lab8.Data;

public partial class RailwayTicketsContext : DbContext
{
    public RailwayTicketsContext()
    {
    }

    public RailwayTicketsContext(DbContextOptions<RailwayTicketsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Benefit> Benefits { get; set; }

    public virtual DbSet<Discount> Discounts { get; set; }

    public virtual DbSet<Efmigrationshistory> Efmigrationshistories { get; set; }

    public virtual DbSet<Journey> Journeys { get; set; }

    public virtual DbSet<JourneySegment> JourneySegments { get; set; }

    public virtual DbSet<JourneySegment1> JourneySegments1 { get; set; }

    public virtual DbSet<JourneyService> JourneyServices { get; set; }

    public virtual DbSet<JourneyStation> JourneyStations { get; set; }

    public virtual DbSet<Passenger> Passengers { get; set; }

    public virtual DbSet<Passenger1> Passengers1 { get; set; }

    public virtual DbSet<PassengerDiscount> PassengerDiscounts { get; set; }

    public virtual DbSet<PassengerSpending> PassengerSpendings { get; set; }

    public virtual DbSet<Payment> Payments { get; set; }

    public virtual DbSet<PopularDiscount> PopularDiscounts { get; set; }

    public virtual DbSet<PrintedTicket> PrintedTickets { get; set; }

    public virtual DbSet<Service> Services { get; set; }

    public virtual DbSet<Soldedticket> Soldedtickets { get; set; }

    public virtual DbSet<Station> Stations { get; set; }

    public virtual DbSet<Ticket> Tickets { get; set; }

    public virtual DbSet<UnpopularService> UnpopularServices { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Benefit>(entity =>
        {
            entity.HasKey(e => e.Name).HasName("PRIMARY");

            entity.ToTable("benefits");

            entity.HasIndex(e => e.PercentDiscount, "idx_benefit_discount");

            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Description)
                .HasMaxLength(45)
                .HasColumnName("description");
            entity.Property(e => e.PercentDiscount)
                .HasPrecision(3)
                .HasColumnName("percentDiscount");
        });

        modelBuilder.Entity<Discount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("discount");

            entity.HasIndex(e => e.Name, "idx_discount_name");

            entity.Property(e => e.EndDate)
                .HasColumnType("date")
                .HasColumnName("endDate");
            entity.Property(e => e.StartDate)
                .HasColumnType("date")
                .HasColumnName("startDate");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.PercentDiscount)
                .HasPrecision(3)
                .HasColumnName("percentDiscount");

        });

        modelBuilder.Entity<Efmigrationshistory>(entity =>
        {
            entity.HasKey(e => e.MigrationId).HasName("PRIMARY");

            entity.ToTable("__efmigrationshistory");

            entity.Property(e => e.MigrationId).HasMaxLength(150);
            entity.Property(e => e.ProductVersion).HasMaxLength(32);
        });

        modelBuilder.Entity<Journey>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("journey");

            entity.HasIndex(e => e.Number, "idx_journey_number");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Cars).HasColumnName("cars");
            entity.Property(e => e.Number)
                .HasMaxLength(45)
                .HasColumnName("number");
            entity.Property(e => e.Seats).HasColumnName("seats");
            entity.Property(e => e.Time)
                .HasColumnType("time")
                .HasColumnName("time");
            entity.Property(e => e.TimeDeparture)
                .HasColumnType("time")
                .HasColumnName("timeDeparture");
        });

        modelBuilder.Entity<JourneySegment>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("journey-segment");

            entity.HasIndex(e => e.JourneyId, "journeyId_idx");

            entity.HasIndex(e => e.StationArrivalId, "stationArrivalId_idx");

            entity.HasIndex(e => e.StationDepartureId, "stationDepartureId_idx");

            entity.Property(e => e.Distance).HasColumnName("distance");
            entity.Property(e => e.Duration)
                .HasPrecision(3)
                .HasColumnName("duration");
            entity.Property(e => e.JourneyId).HasColumnName("journeyID");
            entity.Property(e => e.StationArrivalId).HasColumnName("stationArrivalId");
            entity.Property(e => e.StationDepartureId).HasColumnName("stationDepartureId");

            entity.HasOne(d => d.Journey).WithMany()
                .HasForeignKey(d => d.JourneyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("IDjourney");

            entity.HasOne(d => d.StationArrival).WithMany()
                .HasForeignKey(d => d.StationArrivalId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("IDstationArrival");

            entity.HasOne(d => d.StationDeparture).WithMany()
                .HasForeignKey(d => d.StationDepartureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("IDstationDeparture");
        });

        modelBuilder.Entity<JourneySegment1>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("journey-segments");

            entity.Property(e => e.ArrivalCity)
                .HasMaxLength(45)
                .HasColumnName("arrivalCity");
            entity.Property(e => e.BreakOnArrival).HasColumnName("breakOnArrival");
            entity.Property(e => e.BreakOnDeparture).HasColumnName("breakOnDeparture");
            entity.Property(e => e.Cost).HasColumnName("cost");
            entity.Property(e => e.DepartureCity)
                .HasMaxLength(45)
                .HasColumnName("departureCity");
            entity.Property(e => e.JourneyId).HasColumnName("journeyID");
            entity.Property(e => e.SeatsAvailable).HasColumnName("seatsAvailable");
            entity.Property(e => e.TimeOfArrival)
                .HasColumnType("datetime")
                .HasColumnName("timeOfArrival");
            entity.Property(e => e.TimeOfDeparture)
                .HasColumnType("datetime")
                .HasColumnName("timeOfDeparture");
            entity.Property(e => e.TrainNumber)
                .HasMaxLength(45)
                .HasColumnName("trainNumber");
        });

        modelBuilder.Entity<JourneyService>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("journey-services");

            entity.HasIndex(e => e.JorneyId, "flightId_idx");

            entity.HasIndex(e => e.ServiceId, "serviceId_idx");

            entity.Property(e => e.JorneyId).HasColumnName("jorneyId");
            entity.Property(e => e.ServiceId).HasColumnName("serviceId");

            entity.HasOne(d => d.Jorney).WithMany()
                .HasForeignKey(d => d.JorneyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("jorney_ID");

            entity.HasOne(d => d.Service).WithMany()
                .HasForeignKey(d => d.ServiceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("serviceId");
        });

        modelBuilder.Entity<JourneyStation>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("journey-station");

            entity.HasIndex(e => e.Idjourney, "idFlight_idx");

            entity.HasIndex(e => e.Idstation, "idStation_idx");

            entity.Property(e => e.BreakDuration).HasColumnName("breakDuration");
            entity.Property(e => e.Idjourney).HasColumnName("idjourney");
            entity.Property(e => e.Idstation).HasColumnName("idstation");
            entity.Property(e => e.Tracks).HasColumnName("tracks");

            entity.HasOne(d => d.IdjourneyNavigation).WithMany()
                .HasForeignKey(d => d.Idjourney)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("idJorney");

            entity.HasOne(d => d.IdstationNavigation).WithMany()
                .HasForeignKey(d => d.Idstation)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("idStation");
        });

        modelBuilder.Entity<Passenger>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("passenger");

            entity.HasIndex(e => e.Benefit, "benefit_idx");

            entity.HasIndex(e => e.Id, "idPassenger_UNIQUE").IsUnique();

            entity.HasIndex(e => new { e.Name, e.Lastname }, "idx_passenger_name_lastname");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Benefit)
                .HasMaxLength(45)
                .HasColumnName("benefit");
            entity.Property(e => e.Lastname)
                .HasMaxLength(45)
                .HasColumnName("lastname");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");

            entity.HasOne(d => d.BenefitNavigation).WithMany(p => p.Passengers)
                .HasForeignKey(d => d.Benefit)
                .HasConstraintName("benefit");
        });

        modelBuilder.Entity<Passenger1>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("passengers");

            entity.Property(e => e.Benefit)
                .HasMaxLength(45)
                .HasColumnName("benefit");
            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Lastname)
                .HasMaxLength(45)
                .HasColumnName("lastname");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
        });

        modelBuilder.Entity<PassengerDiscount>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("passenger-discount");

            entity.HasIndex(e => e.DiscountId, "discountId_idx");

            entity.HasIndex(e => e.PassengerId, "passengerId_idx");

            entity.Property(e => e.DiscountId).HasColumnName("discountId");
            entity.Property(e => e.PassengerId).HasColumnName("passengerId");

            entity.HasOne(d => d.Discount).WithMany()
                .HasForeignKey(d => d.DiscountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("discountId");

            entity.HasOne(d => d.Passenger).WithMany()
                .HasForeignKey(d => d.PassengerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("passenger_Discount");
        });

        modelBuilder.Entity<PassengerSpending>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("passenger_spending");

            entity.Property(e => e.Lastname)
                .HasMaxLength(45)
                .HasColumnName("lastname");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.NumTickets).HasColumnName("numTickets");
            entity.Property(e => e.Totalspent)
                .HasPrecision(32)
                .HasColumnName("totalspent");
        });

        modelBuilder.Entity<Payment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("payment");

            entity.HasIndex(e => e.TicketId, "ticketId_idx");

            entity.Property(e => e.DateAndTime)
                .HasColumnType("datetime")
                .HasColumnName("dateAndTime");
            entity.Property(e => e.PaymentMethod)
                .HasMaxLength(45)
                .HasColumnName("paymentMethod");
            entity.Property(e => e.TicketId).HasColumnName("ticketId");

            entity.HasOne(d => d.Ticket).WithMany(p => p.Payments)
                .HasForeignKey(d => d.TicketId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("ticketId");
        });

        modelBuilder.Entity<PopularDiscount>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("popular_discounts");

            entity.Property(e => e.DiscountId).HasColumnName("discountId");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.UsageCount).HasColumnName("usageCount");
        });

        modelBuilder.Entity<PrintedTicket>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("printed_ticket");

            entity.Property(e => e.ArrivalCity)
                .HasMaxLength(45)
                .HasColumnName("arrivalCity");
            entity.Property(e => e.Car).HasColumnName("car");
            entity.Property(e => e.Cost).HasColumnName("cost");
            entity.Property(e => e.DepartureCity)
                .HasMaxLength(45)
                .HasColumnName("departureCity");
            entity.Property(e => e.Lastname)
                .HasMaxLength(45)
                .HasColumnName("lastname");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.PaymentMethod)
                .HasMaxLength(45)
                .HasColumnName("paymentMethod");
            entity.Property(e => e.Seat).HasColumnName("seat");
            entity.Property(e => e.TimeArrival)
                .HasColumnType("datetime")
                .HasColumnName("timeArrival");
            entity.Property(e => e.TimeDeparture)
                .HasColumnType("datetime")
                .HasColumnName("timeDeparture");
            entity.Property(e => e.TrainNumber)
                .HasMaxLength(45)
                .HasColumnName("trainNumber");
        });

        modelBuilder.Entity<Service>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("services");

            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Soldedticket>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("soldedticket");

            entity.Property(e => e.ArrivalCity)
                .HasMaxLength(45)
                .HasColumnName("arrivalCity");
            entity.Property(e => e.DepartureCity)
                .HasMaxLength(45)
                .HasColumnName("departureCity");
            entity.Property(e => e.SoldTicket).HasColumnName("soldTicket");
            entity.Property(e => e.Totalspent)
                .HasPrecision(32)
                .HasColumnName("totalspent");
        });

        modelBuilder.Entity<Station>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("station");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.City)
                .HasMaxLength(45)
                .HasColumnName("city");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Tracks).HasColumnName("tracks");
        });

        modelBuilder.Entity<Ticket>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("ticket");

            entity.HasIndex(e => e.JorneyId, "flightId_idx");

            entity.HasIndex(e => e.PassengerId, "passengerId_idx");

            entity.HasIndex(e => e.StationArrivalId, "stationArrivalId_idx");

            entity.HasIndex(e => e.StationDepartureId, "stationId_idx");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Car).HasColumnName("car");
            entity.Property(e => e.Cost).HasColumnName("cost");
            entity.Property(e => e.JorneyId).HasColumnName("jorneyId");
            entity.Property(e => e.PassengerId).HasColumnName("passengerId");
            entity.Property(e => e.Seat).HasColumnName("seat");
            entity.Property(e => e.StationArrivalId).HasColumnName("stationArrivalId");
            entity.Property(e => e.StationDepartureId).HasColumnName("stationDepartureId");
            entity.Property(e => e.TimeArrival)
                .HasColumnType("datetime")
                .HasColumnName("timeArrival");
            entity.Property(e => e.TimeDeparture)
                .HasColumnType("datetime")
                .HasColumnName("timeDeparture");

            entity.HasOne(d => d.Jorney).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.JorneyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("journeyId");

            entity.HasOne(d => d.Passenger).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.PassengerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("passengerId");

            entity.HasOne(d => d.StationArrival).WithMany(p => p.TicketStationArrivals)
                .HasForeignKey(d => d.StationArrivalId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("stationArrivalId");

            entity.HasOne(d => d.StationDeparture).WithMany(p => p.TicketStationDepartures)
                .HasForeignKey(d => d.StationDepartureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("stationDepartureId");
        });

        modelBuilder.Entity<UnpopularService>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("unpopular_services");

            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.ServiceId).HasColumnName("serviceId");
            entity.Property(e => e.UsageCount).HasColumnName("usageCount");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
