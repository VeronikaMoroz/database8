﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class PrintedTicket
{
    public string Name { get; set; } = null!;

    public string Lastname { get; set; } = null!;

    public int Cost { get; set; }

    public string TrainNumber { get; set; } = null!;

    public string DepartureCity { get; set; } = null!;

    public string ArrivalCity { get; set; } = null!;

    public DateTime TimeDeparture { get; set; }

    public DateTime TimeArrival { get; set; }

    public int Car { get; set; }

    public int Seat { get; set; }

    public string PaymentMethod { get; set; } = null!;
}
