﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Benefit
{
    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public decimal PercentDiscount { get; set; }

    public virtual ICollection<Passenger> Passengers { get; set; } = new List<Passenger>();
}
