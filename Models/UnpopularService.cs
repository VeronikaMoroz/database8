﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class UnpopularService
{
    public int ServiceId { get; set; }

    public string Name { get; set; } = null!;

    public long UsageCount { get; set; }
}
