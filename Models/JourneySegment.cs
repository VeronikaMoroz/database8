﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class JourneySegment
{
    public int JourneyId { get; set; }

    public int StationDepartureId { get; set; }

    public int StationArrivalId { get; set; }

    public decimal Duration { get; set; }

    public int Distance { get; set; }

    public virtual Journey Journey { get; set; } = null!;

    public virtual Station StationArrival { get; set; } = null!;

    public virtual Station StationDeparture { get; set; } = null!;
}
