﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class JourneySegment1
{
    public int JourneyId { get; set; }

    public string TrainNumber { get; set; } = null!;

    public string DepartureCity { get; set; } = null!;

    public string ArrivalCity { get; set; } = null!;

    public DateTime TimeOfDeparture { get; set; }

    public int BreakOnDeparture { get; set; }

    public DateTime TimeOfArrival { get; set; }

    public int BreakOnArrival { get; set; }

    public int SeatsAvailable { get; set; }

    public int Cost { get; set; }
}
