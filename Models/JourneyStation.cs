﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class JourneyStation
{
    public int Idjourney { get; set; }

    public int Idstation { get; set; }

    public int BreakDuration { get; set; }

    public int Tracks { get; set; }

    public virtual Journey IdjourneyNavigation { get; set; } = null!;

    public virtual Station IdstationNavigation { get; set; } = null!;
}
