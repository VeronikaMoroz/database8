﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Passenger
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Lastname { get; set; } = null!;

    public string? Benefit { get; set; }

    public virtual Benefit? BenefitNavigation { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
