﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class JourneyService
{
    public int JorneyId { get; set; }

    public int ServiceId { get; set; }

    public virtual Journey Jorney { get; set; } = null!;

    public virtual Service Service { get; set; } = null!;
}
