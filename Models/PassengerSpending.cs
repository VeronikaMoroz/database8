﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class PassengerSpending
{
    public string Name { get; set; } = null!;

    public string Lastname { get; set; } = null!;

    public long NumTickets { get; set; }

    public decimal? Totalspent { get; set; }
}
