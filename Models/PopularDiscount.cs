﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class PopularDiscount
{
    public int DiscountId { get; set; }

    public string Name { get; set; } = null!;

    public long UsageCount { get; set; }
}
