﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Payment
{
    public int Id { get; set; }

    public int TicketId { get; set; }

    public string PaymentMethod { get; set; } = null!;

    public DateTime DateAndTime { get; set; }

    public virtual Ticket Ticket { get; set; } = null!;
}
