﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Station
{
    public int Id { get; set; }

    public string City { get; set; } = null!;

    public string Name { get; set; } = null!;

    public int Tracks { get; set; }

    public virtual ICollection<Ticket> TicketStationArrivals { get; set; } = new List<Ticket>();

    public virtual ICollection<Ticket> TicketStationDepartures { get; set; } = new List<Ticket>();
}
