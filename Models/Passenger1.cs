﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Passenger1
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Lastname { get; set; } = null!;

    public string? Benefit { get; set; }
}
