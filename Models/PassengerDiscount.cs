﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class PassengerDiscount
{
    public int PassengerId { get; set; }

    public int DiscountId { get; set; }

    public virtual Discount Discount { get; set; } = null!;

    public virtual Passenger Passenger { get; set; } = null!;
}
