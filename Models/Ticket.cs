﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Ticket
{
    public int Id { get; set; }

    public int PassengerId { get; set; }

    public DateTime TimeDeparture { get; set; }

    public DateTime TimeArrival { get; set; }

    public int Cost { get; set; }

    public int JorneyId { get; set; }

    public int StationDepartureId { get; set; }

    public int StationArrivalId { get; set; }

    public int Car { get; set; }

    public int Seat { get; set; }

    public virtual Journey Jorney { get; set; } = null!;

    public virtual Passenger Passenger { get; set; } = null!;

    public virtual ICollection<Payment> Payments { get; set; } = new List<Payment>();

    public virtual Station StationArrival { get; set; } = null!;

    public virtual Station StationDeparture { get; set; } = null!;
}
