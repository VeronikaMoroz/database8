﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Soldedticket
{
    public string DepartureCity { get; set; } = null!;

    public string ArrivalCity { get; set; } = null!;

    public long SoldTicket { get; set; }

    public decimal? Totalspent { get; set; }
}
