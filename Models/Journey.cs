﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Journey
{
    public int Id { get; set; }

    public string Number { get; set; } = null!;

    public TimeSpan TimeDeparture { get; set; }

    public int Seats { get; set; }

    public int Cars { get; set; }

    public TimeSpan? Time { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
