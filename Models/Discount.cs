﻿using System;
using System.Collections.Generic;

namespace lab8.Models;

public partial class Discount
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public decimal PercentDiscount { get; set; }
}
