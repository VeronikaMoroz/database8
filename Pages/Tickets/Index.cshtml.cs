﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using lab8.Data;
using lab8.Models;

namespace lab8.Pages.Tickets
{
    public class IndexModel : PageModel
    {
        private readonly lab8.Data.RailwayTicketsContext _context;

        public IndexModel(lab8.Data.RailwayTicketsContext context)
        {
            _context = context;
        }

        public IList<Ticket> Ticket { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Tickets != null)
            {
                Ticket = await _context.Tickets
                .Include(t => t.Jorney)
                .Include(t => t.Passenger)
                .Include(t => t.StationArrival)
                .Include(t => t.StationDeparture).ToListAsync();
            }
        }
    }
}
