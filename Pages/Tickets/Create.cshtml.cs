﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lab8.Data;
using lab8.Models;
using System.Data;

namespace lab8.Pages.Tickets
{
    public class CreateModel : PageModel
    {
        private readonly RailwayTicketsContext _context;

        public CreateModel(RailwayTicketsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["JorneyId"] = new SelectList(_context.Journeys, "Id", "Id");
            ViewData["PassengerId"] = new SelectList(_context.Passengers, "Id", "Id");
            ViewData["StationArrivalId"] = new SelectList(_context.Stations, "Id", "Id");
            ViewData["StationDepartureId"] = new SelectList(_context.Stations, "Id", "Id");

            return Page();
        }

        [BindProperty]
        public Ticket Ticket { get; set; } = default!;

        [BindProperty]
        public Payment Payment { get; set; } = default!;

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (Ticket == null)
            {
                return Page();
            }

            using (var transaction = _context.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    Ticket.Passenger = await _context.Passengers.FindAsync(Ticket.PassengerId);
                    Ticket.Jorney = await _context.Journeys.FindAsync(Ticket.JorneyId);
                    Ticket.StationArrival = await _context.Stations.FindAsync(Ticket.StationArrivalId);
                    Ticket.StationDeparture = await _context.Stations.FindAsync(Ticket.StationDepartureId);

                    _context.Tickets.Add(Ticket);
                    _context.SaveChanges();


                    var payment = new Payment
                    {
                        Id = Ticket.PassengerId,
                        TicketId = Ticket.Id,
                        PaymentMethod = "cash",
                        DateAndTime = DateTime.Now,
                        //Payment.Ticket = Ticket;
                    };

                    _context.Payments.Add(payment);
                    _context.SaveChanges();

                    transaction.Commit();

                    return RedirectToPage("./Index");
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
